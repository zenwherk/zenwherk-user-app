export const categories = [
  {
    value: 1,
    title: 'Cafeterías',
  },
  {
    value: 2,
    title: 'Biblioteca',
  },
  {
    value: 3,
    title: 'Co-Working',
  },
];

export const features = [
  {
    value: 1,
    title: 'Wi-Fi',
  },
  {
    value: 2,
    title: 'Comida',
  },
  {
    value: 3,
    title: 'Estacionamiento',
  },
  {
    value: 4,
    title: 'Zona para fumadores',
  },
  {
    value: 5,
    title: 'Acepta tarjeta'
  },
  {
    value: 6,
    title: 'Cierra tarde'
  },
  {
    value: 7,
    title: 'Estacionamiento para bicicletas'
  },
  {
    value: 8,
    title: 'Ethernet'
  }
];
